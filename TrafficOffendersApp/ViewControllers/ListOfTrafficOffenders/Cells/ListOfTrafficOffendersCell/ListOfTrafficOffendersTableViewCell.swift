//
//  ListOfTrafficOffendersTableViewCell.swift
//  TrafficOffendersApp
//
//  Created by Яков on 20.10.2021.
//

import UIKit

class ListOfTrafficOffendersTableViewCell: UITableViewCell {

    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var nameDriverLabel: UILabel!
    @IBOutlet weak var carPhoto: UIImageView!
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var yearOfManufactureLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var articleOfTheOffenseLabel: UILabel!
    @IBOutlet weak var fineLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusImage.isHidden = true
        statusImage.image = UIImage(named: "closed")
    }
    
    func update(_ offender: TraficOffender) {
        statusImage.isHidden = offender.status
        
        carPhoto.image = UIImage(named: offender.photoFixation ?? "default")
        nameDriverLabel.text = offender.surNameDriver + " " + offender.nameDriver
        licensePlateLabel.text = offender.carDescription.licensePlate
        brandLabel.text = offender.carDescription.brand
        modelLabel.text = offender.carDescription.model
        yearOfManufactureLabel.text = offender.carDescription.yearOfManufactureString
        colorLabel.text = offender.carDescription.color ?? "-"
        articleOfTheOffenseLabel.text = offender.articleOfTheOffense
        fineLabel.text = offender.fineString

    }
}
