//
//  TraficOffenderModel.swift
//  TrafficOffendersApp
//
//  Created by Яков on 18.10.2021.
//

import Foundation

struct CarDescription {
    
    var licensePlate: String
    var model: String
    var brand: String
    var color: String?
    var yearOfManufacture: Int
    
    var yearOfManufactureString: String {
        return "\(yearOfManufacture)"
    }
}

struct TraficOffender {
    
    var idOffense: Int
    var nameDriver: String
    var surNameDriver: String
    var carDescription: CarDescription
    var articleOfTheOffense: String
    var fine: Int?
    var photoFixation: String?
    var status: Bool = true
    
    var fineString: String {
        return fine == nil ? "0" : "\(fine!) грн."
    }
}
