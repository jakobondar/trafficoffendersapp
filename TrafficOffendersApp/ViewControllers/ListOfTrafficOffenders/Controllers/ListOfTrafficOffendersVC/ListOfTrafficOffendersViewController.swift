//
//  ListOfTrafficOffendersViewController.swift
//  TrafficOffendersApp
//
//  Created by Яков on 18.10.2021.
//

import UIKit

class ListOfTrafficOffendersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let listOfTrafficOffendersCellID = "ListOfTrafficOffendersTableViewCell"
    private let cellHeightSectionName: CGFloat = 35
    var sectionName: String = "Список порушників ПДР:"

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: listOfTrafficOffendersCellID, bundle: nil), forCellReuseIdentifier: listOfTrafficOffendersCellID)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TraficOffendersManager.shared.traficOffenders.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: cellHeightSectionName))
        let label = UILabel(frame: view.frame)
        label.backgroundColor = .systemBlue
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 6
        label.font = UIFont.boldSystemFont(ofSize:UIFont.labelFontSize)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = sectionName
        label.textColor = .white
        label.textAlignment = .center
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listOfTrafficOffendersCellID, for: indexPath) as! ListOfTrafficOffendersTableViewCell
        cell.update(TraficOffendersManager.shared.traficOffenders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            TraficOffendersManager.shared.creationListOfCanceledOffense(TraficOffendersManager.shared.traficOffenders[indexPath.row])
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
