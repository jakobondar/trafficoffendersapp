//
//  RecordingOfNewOffendersViewController.swift
//  TrafficOffendersApp
//
//  Created by Яков on 18.10.2021.
//

import UIKit

class RecordingOfNewOffendersViewController: UIViewController {

    var generateID = 0
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var surNameDriverTextField: UITextField!
    @IBOutlet weak var nameDriverTextField: UITextField!
    @IBOutlet weak var brandCarTextField: UITextField!
    @IBOutlet weak var modelCarTextField: UITextField!
    @IBOutlet weak var yearOfCarProductionTextField: UITextField!
    @IBOutlet weak var colorCarTextField: UITextField!
    @IBOutlet weak var plateNumberCarTextField: UITextField!
    @IBOutlet weak var offenseTextField: UITextField!
    @IBOutlet weak var fineTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        generateID = Int.random(in: 1000...10000)
        idLabel.text = "\(generateID)"
    }
    
    @IBAction func addOffenderAction(_ sender: Any) {
        recordingNewOffender(errorMessage())
    }
    
    func recordingNewOffender(_ checkTextField: Bool) {
        if checkTextField {
            
            let yearOfCarProduction: String = yearOfCarProductionTextField.text ?? "0"
            var yearOfCarProductionInt: Int {
                return Int(yearOfCarProduction) ?? 0
            }

            let newOffender: TraficOffender = .init(idOffense: generateID,
                                                    nameDriver: nameDriverTextField.text ?? "-",
                                                    surNameDriver: surNameDriverTextField.text ?? "-",
                                                    carDescription: CarDescription(licensePlate: plateNumberCarTextField.text ?? "-",
                                                                                   model: modelCarTextField.text ?? "-",
                                                                                   brand: brandCarTextField.text ?? "-",
                                                                                   color: colorCarTextField.text,
                                                                                   yearOfManufacture: yearOfCarProductionInt),
                                                    articleOfTheOffense: offenseTextField.text ?? "-",
                                                    fine: Int(fineTextField.text ?? "0"),
                                                    photoFixation: "default")
            
            TraficOffendersManager.shared.traficOffenders.append(newOffender)
            tabBarController?.selectedIndex = 1
        }
    }
    
    func errorMessage() -> Bool {
        var checkingForEmptyTextField: Bool = false
        
        if !(surNameDriverTextField.text == "") && surNameDriverTextField.text != nil &&
            !(nameDriverTextField.text == "") && nameDriverTextField.text != nil &&
            !(brandCarTextField.text == "") && brandCarTextField.text != nil &&
            !(modelCarTextField.text == "") && modelCarTextField.text != nil &&
            !(yearOfCarProductionTextField.text == "") && yearOfCarProductionTextField.text != nil &&
            !(colorCarTextField.text == "") && colorCarTextField.text != nil &&
            !(plateNumberCarTextField.text == "") && plateNumberCarTextField.text != nil &&
            !(offenseTextField.text == "") && offenseTextField.text != nil &&
            !(fineTextField.text == "") && fineTextField.text != nil {
            checkingForEmptyTextField = true
        } else {
            let alertErrorVC = UIAlertController(title: "Помилка", message: "Будь-ласка, заповніть всі поля!", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Продовжити", style: .cancel, handler: nil)
            alertErrorVC.addAction(cancelAction)
            present(alertErrorVC, animated: true, completion: nil)
        }
        return checkingForEmptyTextField
    }
}
