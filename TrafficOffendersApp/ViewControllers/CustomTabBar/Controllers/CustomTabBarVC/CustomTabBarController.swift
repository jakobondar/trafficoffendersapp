//
//  CustomTabBarController.swift
//  TrafficOffendersApp
//
//  Created by Яков on 18.10.2021.
//

import UIKit

class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 1
    }
}
