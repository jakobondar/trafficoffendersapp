//
//  TraficOffendersManager.swift
//  TrafficOffendersApp
//
//  Created by Яков on 18.10.2021.
//

import Foundation
import UIKit

class TraficOffendersManager {
    
    static let shared = TraficOffendersManager()
    
    var traficOffenders: [TraficOffender] = []
    var listOfCanceledOffense: [TraficOffender] = []
    
    private init () {
        traficOffenders = creatingAListOfOffenders().sorted(by: { $0.surNameDriver < $1.surNameDriver })
        listOfCanceledOffense = traficOffenders.filter{ !$0.status }.sorted(by: { $0.surNameDriver < $1.surNameDriver })
    }
    
    func creationListOfCanceledOffense(_ offense: TraficOffender) {
        listOfCanceledOffense.append(changeOfStatus(offense))
        for (index, item) in traficOffenders.enumerated() {
            if item.idOffense == offense.idOffense {
                traficOffenders.remove(at: index)
            }
        }
    }
    
    func changeOfStatus(_ offense: TraficOffender) -> TraficOffender {
        var nullifiedOffense = offense
        nullifiedOffense.fine = 0
        nullifiedOffense.status = false
        return nullifiedOffense
    }
    
    func creatingAListOfOffenders() -> [TraficOffender] {
        
        let jakob = TraficOffender(idOffense: 8586, nameDriver: "Яків", surNameDriver: "Бондар",
                                   carDescription: CarDescription(licensePlate: "AI 8939 KA", model: "Maxima", brand: "Nissan", color: "зелений", yearOfManufacture: 1995),
                                   articleOfTheOffense: "Ст.122 ч.1\nПеревищення водіями транспортних засобів встановлених обмежень швидкості руху транспортних засобів більш як на 20 кілометрів на годину",
                                   fine: 350, photoFixation: "maxima")
        
        let kirill = TraficOffender(idOffense: 8687, nameDriver: "Кирило", surNameDriver: "Трембовецький",
                                    carDescription: CarDescription(licensePlate: "AA 0000 AA", model: "745", brand: "BMW", yearOfManufacture: 2018),
                                    articleOfTheOffense: "Ст.122 ч.4\nПеревищення встановлених обмежень швидкості руху транспортних засобів більш як на 50 кілометрів на годину.",
                                    fine: 1700, photoFixation: "default")
        
        let vasil = TraficOffender(idOffense: 8878, nameDriver: "Василь", surNameDriver: "Локі",
                                   carDescription: CarDescription(licensePlate: "ВА 0000 КХ", model: "Camry", brand: "Toyota", yearOfManufacture: 2015),
                                   articleOfTheOffense: "Ст.122 ч.4\nПеревищення встановлених обмежень швидкості руху транспортних засобів більш як на 50 кілометрів на годину.",
                                   fine: 1700, photoFixation: "default")
        
        let ivan = TraficOffender(idOffense: 9823, nameDriver: "Іван", surNameDriver: "Іванов",
                                  carDescription: CarDescription(licensePlate: "АІ 0000 КХ", model: "2106", brand: "ВАЗ", color: "білий", yearOfManufacture: 1987),
                                  articleOfTheOffense: """
        Ст.130 ч.1
        а) керування транспортними засобами особами в стані алкогольного, наркотичного чи іншого сп'яніння або під впливом лікарських препаратів, що знижують їх увагу та швидкість реакції;
        б) передача керування транспортним засобом особі, яка перебуває в стані такого сп'яніння чи під впливом таких лікарських препаратів;
        в) відмова особи, яка керує транспортним засобом, від проходження огляду на стан сп'яніння або щодо вживання таких лікарських препаратів.
        """,
                                  fine: 17000, photoFixation: "vaz2106")
        
        return [jakob, kirill, ivan, vasil]
    }
}
